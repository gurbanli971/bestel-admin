export default {
  methods: {
    deletable(callback) {
      this.$bvModal.msgBoxConfirm(this.$t("confirm_deletion"), {
        size: "sm",
        centered: true,
        bodyClass: "text-center",
        footerClass: "flex-row-reverse justify-content-center",
        cancelVariant: "danger",
        cancelTitle: this.$t("reject"),
        okTitle: this.$t("submit")
      })
        .then((value) => {
          value && callback();
        });
    },
  }
}
