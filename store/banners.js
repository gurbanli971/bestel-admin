import { findItemIndex } from "@/store/utils";

export const state = () => ({
  active_tab: 0,
  filters: {
    type: [
      { label: "all", value: "all" },
      { label: "active", value: "active" },
      { label: "inactive", value: "inactive" },
    ]
  },
  data: {
    fields: [],
    items: {
      main: [],
      right: [],
      page: [],
      category: []
    }
  },
  item: {
    id: 0,
    order: 0,
    position: "main",
    url: "",
    image: { id: "", file: "" },
    start_date: "",
    end_date: "",
    always: 0,
    pages: 0,
    display: 0
  }
})

export const getters = {
  getActiveTab: ({ active_tab }) => active_tab,
  getBanners: ({ data }) => data,
  getBanner: ({ item }) => item,
  getFilters: ({ filters }) => filters,
}

export const mutations = {
  SET_ACTIVE_TAB(state, value) {
    state.active_tab = value;
  },

  SET_BANNERS({ data }, { type, items }) {
    data.items[type] = items;
  },

  DELETE_BANNER({ data }, { position, id }) {
    const index = findItemIndex(data.items[position], id);
    data.items[position].splice(index, 1);
  }
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/banners", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items } = res.data;
        commit("SET_FIELDS", { module: "banners", fields }, { root: true });
        commit("SET_ITEMS", { module: "banners", items }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error));
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/banners/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "banners", item: res.data }, { root: true });
      })
      .catch(error => console.log(error));
  },

  create({ commit }, form_data) {
    return this.$axios
      .post("/banners/store", form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  sort({ commit, dispatch }, data) {
    return this.$axios
      .post("/banners/reorder", data)
      .then(() => {
        dispatch("fetchAll");
        return Promise.resolve()
      })
      .catch(error => Promise.reject(error));
  },

  update({ commit }, { id, form_data }) {
    return this.$axios
      .post(`/banners/${id}`, form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  toggle(_, { id, display }) {
    return this.$axios
      .post(`/banners/${id}/display`, { display })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  delete({ commit }, { position, id }) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .delete(`/banners/${id}`)
      .then(() => {
        commit("DELETE_BANNER", { position, id });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => Promise.reject(error));
  }
}
