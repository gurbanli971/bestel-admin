export const state = () => ({
  data: [
    {
      id: 1,
      name: {
        az: "Çatdırılma",
        ru: "Доставка",
        en: "Delivery",
      },
    },
    {
      id: 2,
      name: {
        az: "Ödəniş",
        ru: "Оплата",
        en: "Payment",
      },
    },
    {
      id: 3,
      name: {
        az: "Zəmanət",
        ru: "Гарантия",
        en: "Guarantee",
      },
    },
    {
      id: 4,
      name: {
        az: "Kredit",
        ru: "Кредит",
        en: "Credit",
      },
    },
  ]
})

export const getters = {
  getFaqCategories: ({ data }) => data,
  getAsOptions: ({ data }, getters, rootState) => {
    return data.map(i => ({
      label: i.name[rootState.locale],
      value: i.id
    }))
  }
}

export const mutations = {
  SET_FAQ_CATEGORIES(state, data) {
    state.data = data;
  }
}

export const actions = {
  fetchAll({ commit }) {
    return this.$axios
      .get("/faq/categories")
      .then(({ data: res }) => {
        commit("SET_FAQ_CATEGORIES", res.data);
      })
      .catch(error => console.log(error))
  },

  create({ dispatch }, form_data) {
    return this.$axios
      .post("/faq/categories/store", form_data)
      .then(() => {
        dispatch("fetchAll");
        return Promise.resolve();
      })
      .catch(error => Promise.reject(error))
  },

  update({ dispatch }, form_data) {
    return this.$axios
      .post(`/faq/categories/${form_data.id}`, form_data)
      .then(() => {
        dispatch("fetchAll");
        return Promise.resolve();
      })
      .catch(error => Promise.reject(error))
  },

  delete({ dispatch }, id) {
    return this.$axios
      .delete(`/faq/categories/${id}`)
      .then(() => {
        dispatch("fetchAll");
      })
      .catch(error => Promise.reject(error));
  }
}
