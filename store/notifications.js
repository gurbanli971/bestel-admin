export const state = () => ({
  data: {
    count: 0,
    notifications: []
  }
})

export const getters = {
  getNotificationsCount: ({data}) => data.count,
  getNotifications: ({data}) => data.notifications
}


export const mutations = {
  SET_NOTIFICATIONS_COUNT({ data }, count) {
    data.count = count;
  },
  SET_NOTIFICATIONS(state, data) {
    state.data = data;
  }
}

export const actions = {
  fetchAll({ commit }) {
    return this.$axios
      .get("/notifications")
      .then(({ data: res }) => {
        commit("SET_NOTIFICATIONS", res.data);
      })
      .catch(error => console.log(error))
  },

  maskAsReaded({ commit }) {
    return this.$axios
      .post("/notifications/mark-as-read")
      .then(() => {
        commit("SET_NOTIFICATIONS_COUNT", 0);
      })
      .catch(error => console.log(error))
  }
}
