export const state = () => ({
  data: {
    fields: [],
    items: []
  },
  item: {
    id: 1,
    display: false,
    menu: false,
    compare: false,
    name: {
      az: "",
      ru: "",
      en: ""
    },
    parent_id: 1
  },
  pagination: {}
})

export const getters = {
  getCategories: ({ data }) => data,
  getCategory: ({ item }) => item,
  getOptions: ({ data }, getters, rootState) => {
    const result = [];
    data.items.map(item => {
      result.push({
        label: item.name[rootState.locale],
        value: item.id,
        parent_id: null
      });

      item.children.map(item => {
        result.push({
          label: item.name[rootState.locale],
          value: item.id,
          parent_id: item.parent_id
        });
      })
    });
    return result;
  },
  getChildOptions: ({ data }, getters, rootState) => {
    const result = [];
    data.items.map(item => {
      item.children.map(item => {
        result.push({
          label: item.name[rootState.locale],
          value: item.id
        })
      })
    })
    return result;
  },
  getParentOptions: ({ data }, getters, rootState) => {
    const result = [{ id: 0, label: "Root", value: null }];
    data.items.map(item => {
      result.push({
        id: item.id,
        label: item.name[rootState.locale],
        value: item.id,
      })
    });
    return result;
  },
}

export const mutations = {
  SET_CATEGORY_ATTRIBUTES(state, data) {
    state.item.attributes = data;
  }
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/categories", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items } = res.data;
        commit("SET_FIELDS", { module: "categories", fields }, { root: true });
        commit("SET_ITEMS", { module: "categories", items }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error));
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/categories/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "categories", item: res.data }, { root: true });
      })
      .catch(error => console.log(error));
  },

  create({ commit }, form_data) {
    return this.$axios
      .post("/categories/store", form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  update({ commit }, { id, form_data }) {
    return this.$axios
      .post(`/categories/${id}`, form_data)
      .then(({ data: res }) => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  sort({ commit }, data) {
    return this.$axios
      .post("/categories/reorder", data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  sortAttributes({ commit }, data) {
    return this.$axios
      .post("/categories/attribute/reorder", data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  delete({ commit, dispatch }, id) {
    return this.$axios
      .delete(`/categories/${id}`)
      .then(() => {
        dispatch("fetchAll");
      })
      .catch(error => console.log(error));
  }
}
