export const state = () => ({
  filters: {
    payment_statuses: [
      { label: "paid", value: true },
      { label: "not_paid", value: false },
    ],
    delivery_statuses: []
  },
  data: {
    fields: [],
    items: []
  },
  item: {},
  pagination: {}
});

export const getters = {
  getFilters: ({ filters }) => filters,
  getOrders: ({ data }) => data,
  getOrder: ({ item }) => item
}

export const mutations = {
  SET_DELIVERY_STATUSES({ filters }, delivery_statuses) {
    filters.delivery_statuses = delivery_statuses;
  }
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/orders", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { filters, fields, items, pagination } = res.data;
        commit("SET_FILTERS", { module: "orders", filters }, { root: true });
        commit("SET_FIELDS", { module: "orders", fields }, { root: true });
        commit("SET_ITEMS", { module: "orders", items }, { root: true });
        commit("SET_PAGINATION", { module: "orders", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error));
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/orders/${id}/details`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "orders", item: res.data.details }, { root: true });
        commit("SET_DELIVERY_STATUSES", res.data.delivery_statuses);
      })
      .catch(error => console.log(error));
  },

  update({ commit }, { id, delivery_status_id }) {
    return this.$axios
      .post(`/orders/${id}/delivery/status`, { id: delivery_status_id })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  }
}
