export const state = () => ({
  data: {
    fields: [],
    items: []
  },
  item: {
    id: "",
    firstname: "",
    lastname: "",
    email: "",
    address: "",
    pastal_code: "",
    phones: [{ value: "" }],
    created_at: "",
    blocked: false
  },
  pagination: {}
})

export const getters = {
  getUsers: ({ data }) => data,
  getUser: ({ item }) => item,
  getOrders: ({ orders }) => orders
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/customers", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items, pagination } = res.data;
        commit("SET_FIELDS", { module: "users", fields }, { root: true });
        commit("SET_ITEMS", { module: "users", items }, { root: true });
        commit("SET_PAGINATION", { module: "users", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error));
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/customers/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "users", item: res.data }, { root: true });
      })
      .catch(error => console.log(error));
  },

  update({ commit }, form_data) {
    let form = form_data
    if(!form.phones[0].value || form.phones[0].value === ""){
      this._vm.$set(form,'phones',[])
    }
    return this.$axios
      .post(`/customers/${form.id}`, form)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },
}
