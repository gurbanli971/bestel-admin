export const state = () => ({
  item: {
    image_1: { id: "", file: "" },
    image_2: { id: "", file: "" },
    description_1: {
      az: "",
      ru: "",
      en: ""
    },
    description_2: {
      az: "",
      ru: "",
      en: ""
    },
  }
})

export const getters = {
  getPageData: ({ item }) => item
}

export const mutations = {
  SET_PAGE_DATA(state, data) {
    state.item = data;
  }
}

export const actions = {
  fetchPageData({ commit }) {
    return this.$axios
      .get("/pages/about-us")
      .then(({ data: res }) => {
        commit("SET_PAGE_DATA", res.data);
      })
      .catch(error => console.log(error))
  },

  updatePageData({ commit }, form_data) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .post("/pages/about-us", form_data)
      .then(() => {
        commit("SET_LOADING", false, { root: true });
        return Promise.resolve();
      })
      .catch(error => Promise.reject(error))
  }
}
