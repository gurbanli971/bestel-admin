export const actions = {
  login(_, form_data) {
    return this.$auth.loginWith("local", { data: form_data });
  },

  updateDetails(_, form_data) {
    return this.$axios
      .post("/auth/details/update", form_data)
      .then(() => this.$auth.fetchUser())
      .catch(error => Promise.reject(error));
  },

  forgotPassword(_, form_data) {
    return this.$axios
      .post("/auth/email", form_data)
      .then(() => Promise.resolve())
      .catch((error) => Promise.reject(error))
  },

  updatePassword(_, form_data) {
    return this.$axios
      .post("/auth/reset-password", form_data)
      .then(() => Promise.resolve())
      .catch((error) => Promise.reject(error))
  }
};
