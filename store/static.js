export const state = () => ({
  page_templates: [],
  data: {
    fields: [],
    items: []
  },
  item: {
    id: 1,
    template: "",
    image: { id: "", file: "" },
    active: 0,
    top_menu: 0,
    bottom_menu: 0,
    external_url: "",
    title: {
      az: "",
      ru: "",
      en: ""
    },
    content: {
      az: "",
      ru: "",
      en: ""
    }
  }
})

export const getters = {
  getPages: ({ data }) => data,
  getPage: ({ item }) => item,
  getPageTemplates: ({ page_templates }) => page_templates
}

export const mutations = {
  SET_PAGE_TEMPLATES(state, templates) {
    state.page_templates = templates;
  }
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/pages", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items } = res.data;
        commit("SET_FIELDS", { module: "static", fields }, { root: true });
        commit("SET_ITEMS", { module: "static", items }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error))
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/pages/${id}`)
      .then(({ data: res }) => {
        const { templates, item } = res.data;
        commit("SET_PAGE_TEMPLATES", templates);
        commit("SET_ITEM", { module: "static", item }, { root: true });
      })
      .catch(error => console.log(error))
  },

  create({ commit }, form_data) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .post("/pages/store", form_data)
      .then(() => {
        commit("SET_LOADING", false, { root: true });
        return Promise.resolve();
      })
      .catch(error => Promise.reject(error))
  },

  update({ commit }, { id, form_data }) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .post(`/pages/${id}`, form_data)
      .then(() => {
        commit("SET_LOADING", false, { root: true });
        return Promise.resolve();
      })
      .catch(error => Promise.reject(error))
  },

  sort({ commit }, data) {
    return this.$axios
      .post("/pages/reorder", data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  toggle(_, { id, active }) {
    return this.$axios
      .post(`/pages/${id}/display`, { active })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  delete({ commit }, id) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .delete(`/pages/${id}`)
      .then(() => {
        commit("REMOVE_TABLE_ITEM", { module: "static", id }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => Promise.reject(error))
  }
}
