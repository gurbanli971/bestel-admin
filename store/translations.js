export const state = () => ({
  data: {
    fields: [],
    items: []
  },
  item: {
    id: 1,
    key: "",
    values: {
      az: "",
      ru: "",
      en: ""
    }
  }
})

export const getters = {
  getTranslations: ({ data }) => data,
  getTranslation: ({ item }) => item
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/translations", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items } = res.data;
        commit("SET_FIELDS", { module: "translations", fields }, { root: true });
        commit("SET_ITEMS", { module: "translations", items }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error))
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/translations/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "translations", item: res.data }, { root: true });
      })
      .catch(error => console.log(error))
  },

  update({ commit }, form_data) {
    return this.$axios
      .post(`/translations/${form_data.id}`, form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error))
  }
}
