import { findItemIndex } from "@/store/utils";

export const state = () => ({
  locale: "",
  loading: false,
  statistics: {}
})

export const getters = {
  getStatistics: ({ statistics }) => statistics
}

export const mutations = {
  SET_LOADING(state, loading) {
    state.loading = loading;
  },

  mutate(state, obj) {
    state[obj.module][obj.property] = obj.value;
  },

  SET_LOCALE(state, locale) {
    state.locale = locale;
  },

  SET_FILTERS(state, { module, filters }) {
    state[module].filters = { ...state[module].filters, ...filters };
  },

  SET_FIELDS(state, { module, fields }) {
    state[module].data.fields = fields;
  },

  SET_ITEMS(state, { module, items }) {
    state[module].data.items = items;
  },

  SET_ITEM(state, { module, item }) {
    state[module].item = item;
  },

  ADD_TABLE_ITEM(state, { module, item }) {
    state[module].data.items.push(item);
  },

  REMOVE_TABLE_ITEM(state, { module, id }) {
    const index = findItemIndex(state[module].data.items, id);
    state[module].data.items.splice(index, 1);
  },

  SET_PAGINATION(state, { module, pagination }) {
    state[module].pagination = pagination;
  },

  SET_PAGINATION_PAGE(state, { module, page }) {
    state[module].pagination.current_page = page;
  },

  SET_DASHBOARD_STATISTICS(state, statistics) {
    state.statistics = statistics;
  }
}

export const actions = {
  fetchStatistics({ commit }) {
    return this.$axios
      .get("/dashboard/statistics")
      .then(({ data: res }) => {
        commit("SET_DASHBOARD_STATISTICS", res.data);
      })
      .catch(error => console.log(error))
  },

  async nuxtServerInit({ state, commit, dispatch }) {
    commit("SET_LOCALE", this.$i18n.locale);

    if (state.auth.loggedIn) {
      await dispatch("notifications/fetchAll");
    }
  }
}
