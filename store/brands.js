import { findItemIndex } from "@/store/utils";

export const state = () => ({
  items: [],
  item: {},
})

export const getters = {
  getBrands: ({ items }) => items,
  getBrand: ({ item }) => item,
  getOptions: ({ items }) => {
    return items.map((item) => ({
      id: item.id,
      label: item.name,
      value: item.id
    }))
  }
}

export const mutations = {
  SET_BRANDS(state, brands) {
    state.items = brands;
  },
  ADD_BRAND({ items }, item) {
    items.push(item);
  },
  DELETE_BRAND({ items }, id) {
    const index = findItemIndex(items, id);
    items.splice(index, 1);
  }
}

export const actions = {
  fetchAll({ commit }) {
    return this.$axios
      .get("/brands")
      .then(({ data: res }) => {
        commit("SET_BRANDS", res.data.brands);
      })
      .catch(error => console.log(error));
  },

  create({ commit }, form_data) {
    const config = { headers: { "Content-Type": "multipart/form-data" } };
    return this.$axios
      .post("/brands/store", form_data, config)
      .then(({ data: res }) => {
        commit("ADD_BRAND", res.data);
      })
      .catch(error => Promise.reject(error));
  },

  update({ commit, dispatch }, { id, form_data }) {
    return this.$axios
      .post(`/brands/${id}`, form_data)
      .then(({ data: res }) => {
        dispatch("fetchAll");
      })
      .catch(error => Promise.reject(error));
  },

  sort({ commit }, data) {
    return this.$axios
      .post("/brands/reorder", data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  delete({ commit }, id) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .delete(`/brands/${id}`)
      .then(() => {
        commit("SET_LOADING", false, { root: true });
        commit("DELETE_BRAND", id);
      })
      .catch(error => Promise.reject(error));
  }
}
