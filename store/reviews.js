export const state = () => ({
  data: {
    fields: [],
    items: []
  },
  pagination: {}
})

export const getters = {
  getReviews: ({ data }) => data
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/reviews", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items, pagination } = res.data;
        commit("SET_FIELDS", { module: "reviews", fields }, { root: true });
        commit("SET_ITEMS", { module: "reviews", items }, { root: true });
        commit("SET_PAGINATION", { module: "reviews", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error))
  },

  answer({ commit, dispatch }, form_data) {
    return this.$axios
      .post(`/reviews/${form_data.id}/reply`, form_data)
      .then(() => {
        dispatch("fetchAll");
      })
      .catch(error => Promise.reject(error));
  },

  toggle(_, { id, active }) {
    return this.$axios
      .post(`/reviews/${id}/display`, { active })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },
}
