import XLSX from "xlsx";

export const state = () => ({
  filename: "",
  filters: {
    result_type: [
      { label: "all", value: "" },
      { label: "db_multiple_stock_id", value: "multiple_stock_id_db" },
      { label: "different_stock_ids", value: "stock_id_different" },
      // { label: "different_part_number", value: "product_number_different" },
      // { label: "wrong_price", value: "price_error" },
      { label: "wrong_count", value: "count_error" },
      { label: "price_raised", value: "priceraise" },
      { label: "price_falled", value: "pricefall" },
      { label: "price_same", value: "pricesame" },
    ],
    count_type: [
      { label: "all", value: "" },
      { label: "count_raised", value: "countraise" },
      { label: "count_falled", value: "countfall" },
      { label: "count_same", value: "countsame" },
    ],
    importable: [
      { label: "all", value: "" },
      { label: "importable", value: "true" },
      { label: "not_importable", value: "false" },
    ],
    on_sale: [
      { label: "all", value: "" },
      { label: "has_discount", value: "has_discount" },
      { label: "in_campaign", value: "in_campaign" },
      { label: "not_in_campaign", value: "not_in_campaign" },
    ]
  },
  data: {
    fields: [],
    items: [],
    latest_import_datetime: ""
  }
})

export const getters = {
  getExcel: ({data}) => data,
  getFilters: ({filters}) => filters,
  getLatestImportDate: ({data}) => data.latest_import_datetime
}


export const mutations = {
  SET_LATEST_IMPORT_DATE({ data }, date) {
    data.latest_import_datetime = date;
  }
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/excel", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items, latest_import_datetime } = res.data;
        commit("SET_FIELDS", { module: "excel", fields }, { root: true });
        commit("SET_ITEMS", { module: "excel", items }, { root: true });
        commit("SET_LATEST_IMPORT_DATE", latest_import_datetime);
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => {
        commit("SET_LOADING", false, { root: true });
        console.log(error)
      })
  },

  upload({ commit }, form_data) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .post("/excel/upload", form_data)
      .then(() => {
        commit("SET_LOADING", false, { root: true });
        return Promise.resolve();
      })
      .catch(error => {
        commit("SET_LOADING", false, { root: true });
        return Promise.reject(error);
      })
  },

  fetchFile() {
    return this.$axios
      .post("/excel/file", null, { responseType: "arraybuffer" })
      .then(({ data: res }) => {
        return new Blob([res], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });
      })
  },

  async read({ commit, dispatch }) {
    commit("SET_LOADING", true, { root: true });

    try {
      const excel_object = {};
      const blob = await dispatch("fetchFile");

      const reader = new FileReader();
      reader.readAsBinaryString(blob);
      reader.onload = (event) => {
        const workbook = XLSX.read(event.target.result, { type: "binary" });

        workbook.SheetNames.forEach(sheet => {
          excel_object[sheet] = XLSX.utils.sheet_to_json(workbook.Sheets[sheet]);
        });

        return this.$axios
          .post("/excel/read", excel_object)
          .then(({ data: res }) => {
            const { fields, items, latest_import_datetime } = res.data;
            commit("SET_FIELDS", { module: "excel", fields }, { root: true });
            commit("SET_ITEMS", { module: "excel", items }, { root: true });
            commit("SET_LATEST_IMPORT_DATE", latest_import_datetime);
            commit("SET_LOADING", false, { root: true });
          })
          .catch(error => {
            commit("SET_LOADING", false, { root: true });
            return Promise.reject(error);
          })
      }
    } catch (error) {
      commit("SET_LOADING", false, { root: true });
      return Promise.reject();
    }
  },

  filterBy({ commit }, value) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(`/excel/filter?value=${value}`)
      .then(({ data: res }) => {
        const { fields, items, latest_import_datetime } = res.data;
        commit("SET_FIELDS", { module: "excel", fields }, { root: true });
        commit("SET_ITEMS", { module: "excel", items }, { root: true });
        commit("SET_LATEST_IMPORT_DATE", latest_import_datetime);
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => {
        commit("SET_LOADING", false, { root: true });
        return Promise.reject(error);
      });
  },

  import({ dispatch }, data) {
    return this.$axios
      .post("/excel/import", data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  umicoShortExport(_, data) {
    if (data && data.length) {
      return this.$axios
        .post("/excel/umico/short", data, { responseType: "arraybuffer" })
        .then(({ data: res }) => {
          const blob = new Blob([res], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          });
          window.open(window.URL.createObjectURL(blob), "_blank");
        })
        .catch(error => Promise.reject(error));
    }
  },

  umicoFullExport(_, data) {
    if (data && data.length) {
      return this.$axios
        .post("/excel/umico/full", data, { responseType: "arraybuffer" })
        .then(({ data: res }) => {
          const blob = new Blob([res], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          });
          window.open(window.URL.createObjectURL(blob), "_blank");
        })
        .catch(error => Promise.reject(error));
    }
  }
}
