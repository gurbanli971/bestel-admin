export const state = () => ({
  filters: {
    arg: [
      { label: "all", value: "all" },
      { label: "active", value: "active" },
      { label: "inactive", value: "inactive" },
      { label: "outdated", value: "outdated" },
    ]
  },
  data: {
    fields: [],
    items: []
  },
  item: {}
})

export const getters = {
  getCampaigns: ({ data }) => data,
  getCampaign: ({ item }) => item,
  getFilters: ({ filters }) => filters,
  getOptions: ({ data }) => {
    return data.items
      .filter((item) => item.active)
      .map((item) => ({
        id: item.id,
        label: item.name,
        value: item.id
      }))
  }
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/campaigns", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items } = res.data;
        commit("SET_FIELDS", { module: "campaigns", fields }, { root: true });
        commit("SET_ITEMS", { module: "campaigns", items }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error));
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/campaigns/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "campaigns", item: res.data }, { root: true });
      })
      .catch(error => Promise.reject(error));
  },

  create({ commit }, form_data) {
    return this.$axios
      .post("/campaigns/store", form_data)
      .then(({ data: res }) => {
        commit("ADD_TABLE_ITEM", { module: "campaigns", item: res.data }, { root: true });
      })
      .catch(error => Promise.reject(error));
  },

  update({ commit, dispatch }, { id, form_data }) {
    return this.$axios
      .post(`/campaigns/${id}`, form_data)
      .then(() => {
        dispatch("fetchAll");
      })
      .catch(error => Promise.reject(error));
  },

  toggle(_, { id, active }) {
    return this.$axios
      .post(`/campaigns/${id}/display`, { active })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  delete({ commit }, id) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .delete(`/campaigns/${id}`)
      .then(() => {
        commit("REMOVE_TABLE_ITEM", { module: "campaigns", id }, { root: true });
        commit("SET_LOADING", false, { root: true })
      })
      .catch(error => Promise.reject(error));
  }
}
