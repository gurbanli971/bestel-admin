export const state = () => ({
  data: {
    fields: [],
    items: []
  },
  item: {
    id: 1,
    name: {
      az: "",
      ru: "",
      en: ""
    },
    address: {
      az: "",
      ru: "",
      en: ""
    },
    map_link: "",
    phones: [{ value: "" }],
    active: true
  }
})

export const getters = {
  getBranches: ({ data }) => data,
  getBranch: ({ item }) => item
}

export const actions = {
  fetchAll({ commit }) {
    return this.$axios
      .get("/stores")
      .then(({ data: res }) => {
        const { fields, items } = res.data;
        commit("SET_FIELDS", { module: "branches", fields }, { root: true });
        commit("SET_ITEMS", { module: "branches", items }, { root: true });
      })
      .catch(error => console.log(error))
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/stores/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "branches", item: res.data }, { root: true });
      })
      .catch(error => console.log(error))
  },

  create({ commit }, form_data) {
    return this.$axios
      .post("/stores/store", form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error))
  },

  update({ commit }, form_data) {
    return this.$axios
      .post(`/stores/${form_data.id}`, form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error))
  },

  toggle(_, { id, active }) {
    return this.$axios
      .post(`/stores/${id}/display`, { active })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  delete({ commit }, id) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .delete(`/stores/${id}`)
      .then(() => {
        commit("REMOVE_TABLE_ITEM", { module: "branches", id }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => Promise.reject(error))
  }
}
