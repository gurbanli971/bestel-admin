export const state = () => ({
  data: {
    fields: [],
    items: []
  },
  pagination: {}
});

export const getters = {
  getComplaints: ({ data }) => data,
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/contact-forms", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items, pagination } = res.data;
        commit("SET_FIELDS", { module: "complaints", fields }, { root: true });
        commit("SET_ITEMS", { module: "complaints", items }, { root: true });
        commit("SET_PAGINATION", { module: "complaints", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error))
  },

  updateStatus({ dispatch }, { complaint_id, status }) {
    return this.$axios
      .post(`/contact-forms/update-status/${complaint_id}`, { status })
      .then(() => dispatch("fetchAll"))
      .catch(error => console.log(error))
  }
}
