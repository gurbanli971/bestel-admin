export const state = () => ({
  filters: {
    link_type: [
      { label: "with_consumable", value: "consumable" },
      { label: "with_accessory", value: "accessory" },
      { label: "with_gift", value: "gift" },
    ]
  },
  data: {
    fields: [],
    items: [],
    attachments: [],
  },
  item: {
    id: null,
    category_id: null,
    brand_id: null,
    part_number: "",
    name: "",
    quantity: null,
    stock_id_1: null,
    stock_id_2: null,
    price: null,
    low_price: null,
    images: [],
    video_url: "",
    description: "",
    campaign_id: null,
    attributes: [],
    attachments: {
      consumable: [],
      accessories: [],
      gifts: []
    }
  },
  pagination: {}
})

export const getters = {
  getProducts: ({ data }) => data,
  getProduct: ({ item }) => item,
  getFilters: ({ filters }) => filters,
  getAttachments: ({ data }) => data.attachments
}

export const mutations = {
  SET_FILTERS(state, filters) {
    state.filters = { ...state.filters, ...filters };
  },
  SET_ATTACHMENTS({ data }, attachments) {
    data.attachments = attachments;
  }
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/products", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { filters, fields, items, pagination } = res.data;
        commit("SET_FILTERS", filters);
        commit("SET_FIELDS", { module: "products", fields }, { root: true });
        commit("SET_ITEMS", { module: "products", items }, { root: true });
        commit("SET_PAGINATION", { module: "products", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error))
  },

  upload({ commit }, form_data) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .post("/products/import", form_data)
      .then(() => {
        commit("SET_LOADING", false, { root: true });
        return Promise.resolve();
      })
      .catch(error => {
        commit("SET_LOADING", false, { root: true });
        return Promise.reject(error);
      })
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/products/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "products", item: res.data }, { root: true });
        if (res.data.attributes.length) {
          commit("attributes/SET_CATEGORY_ATTRIBUTES", res.data.attributes, { root: true });
        }
      })
      .catch(error => console.log(error))
  },

  fetchCopyOne({ commit }, id) {
    return this.$axios
      .get(`/products/${id}/copy`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "products", item: res.data }, { root: true });
        if (res.data.attributes.length) {
          commit("attributes/SET_CATEGORY_ATTRIBUTES", res.data.attributes, { root: true });
        }
      })
      .catch(error => console.log(error))
  },

  create({ commit }, form_data) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .post("/products/store", form_data)
      .then(({ data: res }) => {
        commit("SET_LOADING", false, { root: true });
        return res.data.id;
      })
      .catch(error => {
        commit("SET_LOADING", false, { root: true });
        return Promise.reject(error);
      })
  },

  update({ commit }, { id, form_data }) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .post(`/products/${id}`, form_data)
      .then(() => {
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => {
        commit("SET_LOADING", false, { root: true });
        return Promise.reject(error);
      })
  },

  updateCopy({ commit }, { id, form_data }) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .post(`/products/copy`, form_data)
      .then(() => {
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => {
        commit("SET_LOADING", false, { root: true });
        return Promise.reject(error);
      })
  },

  toggle(_, { id, display }) {
    return this.$axios
      .post(`/products/${id}/display`, { display })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  deleteDocument(_, product_id) {
    return this.$axios
      .delete(`/products/document/${product_id}`)
      .then(() => Promise.resolve())
      .catch((error) => Promise.reject(error))
  },

  export(_, data) {
    if (data && data.length) {
      return this.$axios
        .post("/products/export", data, { responseType: "arraybuffer" })
        .then(({ data: res }) => {
          const blob = new Blob([res], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          });
          window.open(window.URL.createObjectURL(blob), "_blank");
        })
        .catch(error => Promise.reject(error));
    }
  },

  validateField(_, fieldObject) {
    return this.$axios
      .post("/products/validate", fieldObject)
      .then(({ data: res }) => res.data)
      .catch(error => Promise.reject(error));
  },

  searchAsAttachments({ commit }, query) {
    const url = this.$applyQueryToUrl("/products/attachments/search", query);

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        commit("SET_ATTACHMENTS", res.data);
      })
      .catch(error => Promise.reject(error))
  },
}
