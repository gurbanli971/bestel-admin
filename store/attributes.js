import { findItemIndex } from "@/store/utils";

export const state = () => ({
  data: {
    fields: [],
    items: [],
  },
  category_attributes: [],
  attribute_options: [],
  item: {
    name: {
      az: "",
      ru: "",
      en: "",
    },
    category_ids: null,
    unit_of_measurement: "",
    type: "string",
    filterable: false,
    options: []
  },
  pagination: {}
})

export const getters = {
  getAttributes: ({ data }) => data,
  getAttribute: ({ item }) => item,
  getCategoryAttributes: ({ category_attributes }) => category_attributes,
  getAttributeOptions: ({ attribute_options }) => attribute_options
}

export const mutations = {
  SET_CATEGORY_ATTRIBUTES(state, data) {
    state.category_attributes = data;
  },
  SET_ATTRIBUTE_OPTIONS(state, data) {
    state.attribute_options = data;
  },
  CHANGE_ATTRIBUTE_VALUE(state, { id, value }) {
    const index = findItemIndex(state.category_attributes, id);
    state.category_attributes[index].value = value;
  }
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/attributes", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items, pagination } = res.data;
        commit("SET_FIELDS", { module: "attributes", fields }, { root: true });
        commit("SET_ITEMS", { module: "attributes", items }, { root: true });
        commit("SET_PAGINATION", { module: "attributes", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error))
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/attributes/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "attributes", item: res.data }, { root: true });
      })
      .catch(error => console.log(error))
  },

  fetchByCategory({ commit }, category_id) {
    return this.$axios
      .get(`/attributes/category/${category_id}`)
      .then(({ data: res }) => {
        commit("SET_CATEGORY_ATTRIBUTES", res.data);
      })
      .catch(error => console.log(error))
  },

  fetchOptions({ commit }) {
    return this.$axios
      .get("/attributes/select-options")
      .then(({ data: res }) => {
        commit("SET_ATTRIBUTE_OPTIONS", res.data);
      })
      .catch(error => console.log(error))
  },

  fetchByOptionValues({ commit }, { category_id, option_values }) {
    return this.$axios
      .get(`/attributes/category/${category_id}?option_values=${option_values}`)
      .then(({ data: res }) => {
        commit("SET_CATEGORY_ATTRIBUTES", res.data);
      })
      .catch(error => console.log(error))
  },

  create({ commit }, form_data) {
    return this.$axios
      .post("/attributes/store", form_data)
      .then(({ data: res }) => {
        commit("ADD_TABLE_ITEM", { module: "attributes", item: res.data }, { root: true });
      })
      .catch(error => Promise.reject(error))
  },

  update({ commit }, form_data) {
    return this.$axios
      .post(`/attributes/${form_data.id}`, form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error))
  },

  toggle(_, { id, active }) {
    return this.$axios
      .post(`/attributes/${id}/display`, { active })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  delete({ commit }, id) {
    return this.$axios
      .delete(`/attributes/${id}`)
      .then(() => {
        commit("REMOVE_TABLE_ITEM", { module: "attributes", id }, { root: true });
      })
      .catch(error => console.log(error))
  }
}
