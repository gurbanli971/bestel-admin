export const state = () => ({
  data: {
    fields: ["Tarix", "Ad, Soyad", "Email", "Link", "Mehsul"],
    items: []
  },
  pagination: {
    current_page: 1,
    per_page: 20,
    total_count: 40,
    total_pages: 2
  }
})

export const getters = {
  getCheapers: ({ data }) => data,
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/cheaper", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items, pagination } = res.data;
        commit("SET_FIELDS", { module: "cheaper", fields }, { root: true });
        commit("SET_ITEMS", { module: "cheaper", items }, { root: true });
        commit("SET_PAGINATION", { module: "cheaper", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => {
        console.log(error)
        commit("SET_LOADING", false, { root: true });
      })
  },

  updateStatus({ dispatch }, { id, status }) {
    return this.$axios
      .post(`/cheaper/update-status/${id}`, { status })
      .then(() => dispatch("fetchAll"))
      .catch(error => console.log(error))
  }
}
