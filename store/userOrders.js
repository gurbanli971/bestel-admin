export const state = () => ({
  filters: {
    payment_statuses: [
      { label: "paid", value: true },
      { label: "not_paid", value: false },
    ],
  },
  data: {
    fields: [],
    items: []
  },
  pagination: {}
});

export const getters = {
  getFilters: ({ filters }) => filters,
  getOrders: ({ data }) => data,
  getOrder: ({ item }) => item
}

export const actions = {
  fetchAll({ commit }, { id, query = {} }) {
    const url = this.$applyQueryToUrl(`/customers/${id}/orders`, query);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { filters, fields, items, pagination } = res.data;
        commit("SET_FILTERS", { module: "userOrders", filters }, { root: true });
        commit("SET_FIELDS", { module: "userOrders", fields }, { root: true });
        commit("SET_ITEMS", { module: "userOrders", items }, { root: true });
        commit("SET_PAGINATION", { module: "userOrders", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error));
  },
}
