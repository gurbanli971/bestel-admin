export const state = () => ({
  data: {
    fields: [],
    items: []
  },
  item: {}
});

export const getters = {
  getModerators: ({ data }) => data,
  getModerator: ({ item }) => item,
  getAsOptions: ({ data }) => {
    return data.items.map(item => ({
      label: item.name,
      value: item.name,
      id: item.id
    }))
  }
}

export const actions = {
  fetchAll({ commit }) {
    return this.$axios
      .get("/auth/all")
      .then(({ data: res }) => {
        commit("SET_FIELDS", { module: "moderators", fields: res.data.fields }, { root: true });
        commit("SET_ITEMS", { module: "moderators", items: res.data.items }, { root: true });
      })
      .catch(error => console.log(error));
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/auth/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "moderators", item: res.data }, { root: true });
      })
      .catch(error => console.log(error));
  },

  create({ commit }, form_data) {
    return this.$axios
      .post("/auth/store", form_data)
      .then(({ data: res }) => {
        commit("ADD_TABLE_ITEM", { module: "moderators", item: res.data }, { root: true });
      })
      .catch(error => Promise.reject(error));
  },

  update({ commit }, form_data) {
    return this.$axios
      .post(`/auth/${form_data.id}`, form_data)
      .then(() => Promise.resolve())
      .catch(error => console.log(error));
  },

  delete({ commit }, id) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .delete(`/auth/${id}`)
      .then(() => {
        commit("REMOVE_TABLE_ITEM", { module: "moderators", id }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error));
  }
}
