export const state = () => ({
  data: {
    items: [],
  },
  item: {
    id: 1,
    category_id: "",
    title: {
      az: "",
      ru: "",
      en: ""
    },
    content: {
      az: "",
      ru: "",
      en: ""
    },
    active: 1
  }
})

export const getters = {
  getFaqs: ({ data }) => data,
  getFaq: ({ item }) => item,
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/faq", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        commit("SET_ITEMS", { module: "faq", items: res.data.items }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error))
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/faq/${id}`)
      .then(({ data: res }) => {
        const { categories, item } = res.data;
        commit("SET_FAQ_CATEGORIES", categories);
        commit("SET_ITEM", { module: "faq", item }, { root: true });
      })
      .catch(error => console.log(error));
  },

  create({ commit }, form_data) {
    return this.$axios
      .post("/faq/store", form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  update({ commit }, form_data) {
    return this.$axios
      .post(`/faq/${form_data.id}`, form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  toggle(_, { id, active }) {
    return this.$axios
      .post(`/faq/${id}/display`, { active })
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error));
  },

  delete({ commit }, id) {
    return this.$axios
      .delete(`/faq/${id}`)
      .then(() => {
        commit("REMOVE_TABLE_ITEM", { module: "faq", id }, { root: true });
      })
      .catch(error => Promise.reject(error));
  }
}
