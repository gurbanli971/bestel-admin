export const state = () => ({
  data: {
    fields: [],
    items: []
  },
  item: {},
  lastId: null,
  pagination: {}
})

export const getters = {
  getNotes: ({ data }) => data,
  getNote: ({ item }) => item,
}

export const actions = {
  fetchAll({ commit }, filters = {}) {
    const url = this.$applyQueryToUrl("/notes", filters);
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .get(url)
      .then(({ data: res }) => {
        const { fields, items, pagination } = res.data;
        commit("SET_FIELDS", { module: "notes", fields }, { root: true });
        commit("SET_ITEMS", { module: "notes", items }, { root: true });
        commit("SET_PAGINATION", { module: "notes", pagination }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => console.log(error))
  },

  fetchOne({ commit }, id) {
    return this.$axios
      .get(`/notes/${id}`)
      .then(({ data: res }) => {
        commit("SET_ITEM", { module: "notes", item: res.data }, { root: true });
      })
      .catch(error => console.log(error))
  },

  async create({ commit }, form_data) {
    const { data } = await this.$axios.$post("/notes/store", form_data)
    console.log()
    commit('mutate', {
      module:'notes',
      property:'lastId',
      value: data.id
    },{ root: true })
  },

  update({ commit }, form_data) {
    return this.$axios
      .post(`/notes/${form_data.id}`, form_data)
      .then(() => Promise.resolve())
      .catch(error => Promise.reject(error))
  },

  delete({ commit }, id) {
    commit("SET_LOADING", true, { root: true });

    return this.$axios
      .delete(`/notes/${id}`)
      .then(() => {
        commit("REMOVE_TABLE_ITEM", { module: "notes", id }, { root: true });
        commit("SET_LOADING", false, { root: true });
      })
      .catch(error => Promise.reject(error))
  }
}
