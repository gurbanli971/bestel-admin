export default function({ app, $axios }) {
  $axios.onRequest(config => {
    config.headers["locale"] = app.i18n.locale;
  });
}
