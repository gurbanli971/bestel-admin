import Vue from "vue";

import SearchBlock from "@/components/elements/SearchBlock";
import WhitePanel from "@/components/global/WhitePanel";
import DataTable from "@/components/tables/DataTable";
import FormField from "@/components/global/form/FormField";
import NumberField from "@/components/global/form/NumberField";
import FileInputPreview from "@/components/global/form/FileInputPreview";

Vue.component("search-block", SearchBlock);
Vue.component("white-panel", WhitePanel);
Vue.component("data-table", DataTable);
Vue.component("form-field", FormField);
Vue.component("number-field", NumberField);
Vue.component("file-input-preview", FileInputPreview);
